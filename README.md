# Prophet（预言家）
[访问GitHub项目](https://github.com/Miluer-tcq/Prophet) | [访问码云项目](https://gitee.com/t940783074/Prophet)

[中文](README.md) | [English](README.en.md)
## 基于以太坊区块链智能合约技术和Vue框架开发的投票预测平台
实现用户在有成本制约条件下，整体投票结果对预测事件未来走向具有良好的参考价值。同时采用区块链技术，避免开发人员篡改数据的可能，保证了投票数据安全可靠，增强了平台可信度。
- ## [项目已部署，在线Demo](https://t940783074.gitee.io/prophet)
介于国际网络限制，无法直接使用[Infura](https://infura.io/)访问以太坊Dapp，因此平台完整体验采用安装[MetaMask](https://metamask.io/)浏览器插件的解决办法。
## 功能与优化
- [x] 连接区块链
- [x] 创建用户
- [x] 个人信息展示
- [x] 投票事件展示
- [x] 创建事件
- [x] 事件投票
- [x] 事件汇报
- [x] 申请奖励
- [x] 不同颜色展示事件状态
- [x] 展示用户所以操作记录
- [x] Other

## 智能合约数据结构
```
//完整代码见 src/Solidity

contract Ballot{
    struct Event{
        uint ID;//事件ID
        address Sponsor;//发起人 
        string Content;//事件内容 
        string[] Option;//事件选项 
        address Reporter;//汇报人 
        uint StartDate;//开始时间 
        uint Deadline;//截至时间 
        address [] Voters;//投票人 
        uint PrizePool;//奖池 
        bool Report;//汇报状态 默认为false
        uint Result;//事件结果 对应选项索引 默认为99
    }
    
    struct Player{
        address Uesr;//用户地址 
        uint Balance;//用户余额 
        uint[] Created;//已创建ID
        uint[] Selected;//已投票ID
        uint[] Report;//需汇报ID
        mapping(uint=>Option) OptionInfo;//每个已投票事件的选项资金 
    }
    
    struct Option{
        bool Voted;//事件投票状态 
        uint Time; //投票时间
        uint Option;//事件选项 
        uint Finance;//事件金额 
        bool Reward;//事件奖励状态  
    }
    
    struct Finance{
        mapping(uint=>uint) Finance;//选项对应金额 
    }
    
    struct Log{
        uint Time;
        uint ID;
        string action;
        int Change;
        uint Balance;
    }
        

    Event[] Events;//总事件
    mapping(uint=>Finance) EventFinance;//事件对应金额 
    address[] ExistPlayers;//总用户 
    mapping(address=>Player) Players;//用户对应信息 
    mapping(address=>Log[]) PlayerLogs;
```
## 所用技术
- [Ethereum](https://ethereum.org/zh/)
- [Solidity](https://solidity.readthedocs.io/en/v0.6.10/)
- [Web3.js](https://github.com/ethereum/web3.js/)
- [MetaMask](https://metamask.io/)
- VueCli 3.x
- Vue 2.x
- Vuex
- Vue Router
- [Element UI](http://element.eleme.io/#/zh-CN)
- ES6
- Node.js
- axios
- webpack
- 其他

注意：在VueCli 2.x中公共文件放入static文件夹，而在VueCli 3.x版本中请放入public文件夹，否则axios无法找到文件。该项目采用VueCli 3.x。

## 项目简略介绍
- 分类展示事件
- 进度条和颜色表示不同有效期时长的事件
- 提供查看详情按钮
![](img/概览.png)
- 展示事件状态、当前投票信息和所有用户投票纪录等
![](img/事件详情信息.png)
- 分类展示事件
- 汇报事件图标提醒
![](img/个人事件.png)
- 表单验证
- 快捷日期选择
![](img/创建事件.png)
- 创建用户
- 区块链信息展示
![](img/简略个人信息.png)
- 醒目的个人平台代币余额
- 个人操作纪录
![](img/个人详情信息.png)
- 部分小细节（[浏览demo发现更多](https://t940783074.gitee.io/prophet)）
![](img/交易.png)
![](img/交易1.png)


## Project setup
```
npm install
```

## Compiles and hot-reloads for development
```
npm run serve
```

## Compiles and minifies for production
```
npm run build
```

## Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## 开源协议
- 请遵循原作者MIT开源协议
